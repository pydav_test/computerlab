#!/usr/bin/env python

from travo import GitLab
from travo.jupyter_course import JupyterCourse
from travo.script import main
import sys

forge = GitLab("https://gitlab.com")
course = JupyterCourse(forge=forge,
                path="pyDAV_test",
                name="Python for Data Analysis and Visualisation",
                student_dir="./",
                session_path="2023-2024",
                # Additional configuration goes here
                )
course.script = sys.argv[0]

usage = f"""Help for {course.script}
======================================

Download or update an assignment (here for Exam_test):

    {course.script} fetch Exam_test

Submit your assignment (here for Exam_test)

    {course.script} submit Exam_test

More help:

    {course.script} --help
"""


if __name__ == '__main__':
    main(course, usage)
